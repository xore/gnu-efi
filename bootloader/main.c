#include <efi.h>
#include <efilib.h>
#include <elf.h>

typedef unsigned long long size_t;

EFI_FILE* LoadFile(EFI_FILE* m_pDirectory, CHAR16* m_sPath, EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE* m_pSystemTable){
	EFI_FILE* m_pLoadedFile;

	EFI_LOADED_IMAGE_PROTOCOL* m_pLoadedImage;
	m_pSystemTable->BootServices->HandleProtocol(ImageHandle, &gEfiLoadedImageProtocolGuid, (void**)&m_pLoadedImage);

	EFI_SIMPLE_FILE_SYSTEM_PROTOCOL* m_pFileSystem; 
	m_pSystemTable->BootServices->HandleProtocol(m_pLoadedImage->DeviceHandle, &gEfiSimpleFileSystemProtocolGuid, (void**)&m_pFileSystem);

	if(m_pDirectory == NULL){
		m_pFileSystem->OpenVolume(m_pFileSystem, &m_pDirectory);
	}
	
	EFI_STATUS m_nS = m_pDirectory->Open(m_pDirectory, &m_pLoadedFile, m_sPath, EFI_FILE_MODE_READ, EFI_FILE_READ_ONLY);
	if(m_nS != EFI_SUCCESS){
		return NULL;
	}
	
	return m_pLoadedFile;
}

int memcmp(const void* m_pA, const void* m_pB, size_t n){
	const unsigned char* a = m_pA, *b = m_pB;
	
	for(size_t i  = 0; i < n; i++){
		if(a[i] < b[i])return -1;
		else if(a[i] > b[i])return 1;
	}
	
	return 0;
}

EFI_STATUS efi_main (EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable) {

	InitializeLib(ImageHandle, SystemTable);
	Print(L"[xore][efi_main]: booting!\n\r");
	Print(L"[xore][efi_main]: loading kernel.elf!\n\r");

	EFI_FILE* m_pKernelFile = LoadFile(NULL,L"kernel.elf", ImageHandle, SystemTable);

	if(m_pKernelFile == NULL){
		Print(L"[xore][efi_main]: failed to load kernel.elf!\n\r");
	}

	Print(L"[xore][efi_main]: kernel successfuly loaded!\n\r");

	Elf64_Ehdr m_KernelHeader;
	{
		UINTN m_nKernelFileInfoSize;
		EFI_FILE_INFO* m_pKernelFileInfo;
		m_pKernelFile->GetInfo(m_pKernelFile, &gEfiFileInfoGuid, &m_nKernelFileInfoSize, NULL);
		SystemTable->BootServices->AllocatePool(EfiLoaderData, m_nKernelFileInfoSize, (void**)&m_pKernelFileInfo);
		m_pKernelFile->GetInfo(m_pKernelFile, &gEfiFileInfoGuid, &m_nKernelFileInfoSize, (void**)&m_pKernelFileInfo);
		
		UINTN m_nSize = sizeof(m_KernelHeader);
		m_pKernelFile->Read(m_pKernelFile, &m_nSize, &m_KernelHeader);
	

		if(
			memcmp(&m_KernelHeader.e_ident[EI_MAG0], ELFMAG, SELFMAG) != 0 ||
			m_KernelHeader.e_ident[EI_CLASS] != ELFCLASS64 ||
			m_KernelHeader.e_ident[EI_DATA] != ELFDATA2LSB ||
			m_KernelHeader.e_type != ET_EXEC ||
			m_KernelHeader.e_machine != EM_X86_64 ||
			m_KernelHeader.e_version != EV_CURRENT
		){
		
			Print(L"[xore][efi_main]: kernel format is not supported!\n\r");
		
		}

		Print(L"[xore][efi_main]: kernel header successfully verified!\n\r");
	}

	Elf64_Phdr* m_pPhdr;
	{
		m_pKernelFile->SetPosition(m_pKernelFile, m_KernelHeader.e_phoff);
		UINTN m_nSize = m_KernelHeader.e_phnum * m_KernelHeader.e_phentsize;
		SystemTable->BootServices->AllocatePool(EfiLoaderData, m_nSize, (void**)&m_pPhdr);
		m_pKernelFile->Read(m_pKernelFile, &m_nSize, m_pPhdr);
	}

	for(
		Elf64_Phdr* m_pI = m_pPhdr;
		(char*)m_pI < (char*)m_pPhdr + m_KernelHeader.e_phnum * m_KernelHeader.e_phentsize;
		m_pI = (Elf64_Phdr*)((char*)m_pI + m_KernelHeader.e_phentsize)
	){
		switch(m_pI->p_type){
			case PT_LOAD:{
				//								Segments size!
				//								|
				//								^
				int m_nPages = (m_pI->p_memsz + 0x1000 - 1) / 0x1000;
				Elf64_Addr m_Segement = m_pI->p_paddr;
				SystemTable->BootServices->AllocatePages(AllocateAddress, EfiLoaderData, m_nPages, &m_Segement);
				
				m_pKernelFile->SetPosition(m_pKernelFile, m_pI->p_offset);
				UINTN m_nSize = m_pI->p_filesz;
				m_pKernelFile->Read(m_pKernelFile, &m_nSize, (void*)m_Segement);
			break;
			}
		}
	}

	Print(L"[xore][efi_main]: kernel successfully loaded!\n\r");

	int(*k_main)() = ((__attribute__((sysv_abi)) int (*)() ) m_KernelHeader.e_entry);
	
	Print(L"%d\r\n", k_main());

	return EFI_SUCCESS; // Exit the UEFI application
}
